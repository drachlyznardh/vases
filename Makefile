
HOST=docker.io/drachlyznardh
VERSION=1
BINDIR=~/.local/bin/
BINFILE=$(BINDIR)vase

IMAGES=builder pybuilder tester baretester
ENTRYPOINTS=$(addsuffix .entrypoint, $(addprefix containerfile/entrypoint/, $(IMAGES)))

CONTAINERDIR=containerfile
ENTRYINDIR=entrypoint
ENTRYFRAGDIR=$(ENTRYINDIR)/frags
ENTRYOUTDIR=$(CONTAINERDIR)/entrypoint

all: builder pybuilder tester baretester clean install

$(IMAGES): %: $(ENTRYOUTDIR)/%.entrypoint
	@podman build -t $(HOST)/centos7$@:$(VERSION) -f $@.Containerfile $(CONTAINERDIR)
	@#podman push $(HOST)/centos7$@:$(VERSION)

$(ENTRYPOINTS): $(ENTRYOUTDIR)/%.entrypoint: $(ENTRYFRAGDIR)/top.sh $(ENTRYINDIR)/%.root.sh $(ENTRYFRAGDIR)/middle.sh $(ENTRYINDIR)/%.user.sh $(ENTRYFRAGDIR)/bottom.sh
	@cat $(ENTRYFRAGDIR)/top.sh $(ENTRYINDIR)/$*.root.sh $(ENTRYFRAGDIR)/middle.sh $(ENTRYINDIR)/$*.user.sh $(ENTRYFRAGDIR)/bottom.sh > $@

install: vase.in
	@sed -e 's @HOST@ $(HOST) ' -e 's/@VERSION@/$(VERSION)/' vase.in > $(BINFILE)
	@chmod +x $(BINFILE)

clean:
	@podman images -n | grep '<none>' | awk '{ print $$3 }' | while read id; do podman rmi $$id; done

.PHONY: all builder pybuilder tester baretester clean install

