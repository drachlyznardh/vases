
I run on Ubuntu, so I'm following [these
instructions](https://computingforgeeks.com/how-to-install-podman-on-ubuntu/):

```
$> sudo apt update
$> sudo apt -y install software-properties-common
$> sudo add-apt-repository -y ppa:projectatomic/ppa
$> sudo apt update
$> sudo apt -y install podman
 NOPE, DOES NOT WORK…
```

Let's try again with [this other set of
instructions](https://podman.io/getting-started/installation):

```
$> . /etc/os-release
$> sudo sh -c "echo 'deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
$> curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key | sudo apt-key add -
$> sudo apt-get update -qq
$> sudo apt-get -qq -y install podman
```

This worked for me.

