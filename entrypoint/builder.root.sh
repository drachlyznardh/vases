
	echo -n "Copying resources… "
	find /opt/in/specs/ -name \*.spec | while read specfile; do
		cp $specfile ~user/rpmbuild/SPECS
	done
	find /opt/in/src/ -name \*.tgz | while read tgzfile; do
		cp $tgzfile ~user/rpmbuild/SOURCES
	done
	echo Done

	echo "Now calling user…"
	su user -c $0
	echo Done

	cp -R ~user/rpmbuild/RPMS/x86_64/* /opt/out/

