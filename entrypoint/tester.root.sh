
	echo -n "Installing packages… "
	yum install -y /opt/in/rpms/*.rpm
	echo Done

	echo -n "Copying resources… "
	rm -rf ~user/tests/
	cp -aR /opt/in/tests ~user
	chown -R user:user ~user
	echo Done

	echo "Now calling user…"
	su user -c $0
	echo Done

